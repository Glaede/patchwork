package fr.uge.patchwork;

/**
 * This record represent a piece which cost a price (int), has a potential
 * number of time and button and has its representation of it has a an array.
 * 
 * @param price,  the price of piece.
 * @param time,   the number of time on the piece.
 * @param button, the number of button on the piece.
 * @param body,   the representation of the piece has an array.
 * @author Mathieu PERRIOT
 * @author Valentin ISTASSES
 */
public record Piece(int price, int time, int button, int[][] body) {

	/**
	 * This function rotate a piece.
	 * 
	 * @return piece, the new piece rotated.
	 */
	public Piece rotate() {
		var newbody = new int[body[0].length][body.length];
		for (int i = 0; i < body.length; i++) {
			for (int j = 0; j < body[0].length; j++) {
				newbody[j][i] = body[i][j];
			}
		}
		for (int i = 0; i < newbody.length; i++) {
			for (int j = 0; j < newbody[0].length / 2; j++) {
				var tmp = newbody[i][j];
				newbody[i][j] = newbody[i][newbody[0].length - j - 1];
				newbody[i][newbody[0].length - j - 1] = tmp;
			}
		}
		return new Piece(price, time, button, newbody);
	}

	/**
	 * This function mirror a piece.
	 * 
	 * @return piece, the new piece mirrored.
	 */
	public Piece mirror() {
		for (int i = 0; i < body.length; i++) {
			for (int j = 0; j < body[0].length / 2; j++) {
				var tmp = body[i][j];
				body[i][j] = body[i][body[0].length - j - 1];
				body[i][body[0].length - j - 1] = tmp;
			}
		}
		return new Piece(price, time, button, body);
	}

	/**
	 * Used to get the number of bodyparts of a piece.
	 * 
	 * @return int, number of bodyparts.
	 */
	public int length() {
		int tmp = 0;
		for (int i = 0; i < body.length; i++) {
			for (int j = 0; j < body[0].length; j++) {
				if (body[i][j] == 1)
					tmp++;
			}
		}
		return tmp;
	}

	/**
	 * Used to compare two pieces.
	 * 
	 * @param piece, piece to compare to.
	 * @return boolean, true if it is the same.
	 */
	public boolean equals(Piece piece) {
		if (piece.body().equals(body) && piece.button == button && piece.price == price && piece.time == time)
			return true;
		return false;
	}

	/**
	 * This function return the representation of a piece has a string.
	 * 
	 * @return The string representation of a piece.
	 */
	@Override
	public String toString() {
		var builder = new StringBuilder();
		builder.append("price: ").append(price);
		builder.append(" | moves: ").append(time);
		builder.append(" | buttons: ").append(button);
		builder.append("\n");
		for (var i = 0; i < body.length; i++) {
			for (var j = 0; j < body[0].length; j++) {
				if (body[i][j] == 1)
					builder.append("X");
				else
					builder.append(" ");
			}
			builder.append("\n");
		}
		return builder.toString();
	}
}
