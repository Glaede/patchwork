package fr.uge.patchwork;

/**
 * This class coutain a place field which represent a token's place in a board.
 * 
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 */
public class Token {
	private int place;

	/**
	 * Token constructor which initialize a token placed at 0.
	 */
	public Token() {
		place = 0;
	}

	/**
	 * Create a getter to place.
	 * 
	 * @return Value of place (int).
	 */
	public int place() {
		return place;
	}

	/**
	 * Increase the value of place.
	 * 
	 * @param times (int) represent the number of incrementation.
	 */
	public void move(int times) {
		if (times < 0)
			throw new IllegalArgumentException();
		for (var i = 0; i < times; i++) {
			place++;
		}
	}
}
