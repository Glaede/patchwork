package fr.uge.patchwork;
import java.util.Objects;
import java.util.Scanner;

/**
 * This class represent a QuiltBoard which is composed of Piece.
 * 
 * @author Mathieu PERRIOT
 * @author Valentin ISTASSSES
 */
public class QuiltBoard {
	private final int[][] tab;
	private int buttons;

	/**
	 * QuiltBoard Constructor : create a Quiltboard of taille x taille.
	 * 
	 * @param taille, (int) the width/height of a Quiltboard.
	 */
	public QuiltBoard(int taille) {
		buttons = 0;
		tab = new int[taille][taille];
		for (int i = 0; i < taille; i++) {
			for (int y = 0; y < taille; y++) {
				tab[i][y] = 0;
			}
		}
	}

	/**
	 * Add buttons to the board.
	 * 
	 * @param b, number of buttons to add.
	 */
	public void addButtons(int b) {
		buttons += b;
	}

	/**
	 * Getter to the Quiltboard buttons.
	 * 
	 * @return buttons, (int) the number of buttons of the Quiltboard.
	 */
	public int salary() {
		return buttons;
	}

	/**
	 * This function manage input : It place the piece if it's possible to the
	 * coordinate given in argument and add buttons to the QuiltBoard.
	 * 
	 * @param piece_to_place, A Piece to place somewhere in the timeboard
	 * @param scanner,        a Text scanner used to take user instruction such as
	 *                        coordinates.
	 */
	public void placementPhase(Piece piece_to_place, Scanner scanner) {
		Objects.requireNonNull(piece_to_place, "piece to place should not be null");
		Objects.requireNonNull(scanner, "scanner should not be null");

		String input;
		var placed = false;
		do {
			System.out.println(toString() + "\n");
			System.out.println(piece_to_place);

			input = moveInput(scanner);

			if (input.equals("1"))
				piece_to_place = piece_to_place.rotate();
			else if (input.equals("2"))
				piece_to_place = piece_to_place.mirror();
			else {
				placeInput(scanner, piece_to_place);

				placed = true;
				addButtons(piece_to_place.button());
				System.out.println(toString());
			}
		} while (!placed);
	}

	/**
	 * Input used to place the piece from a given String[].
	 * 
	 * @param scanner,        scanner to get input.
	 * @param piece_to_place, the piece to place.
	 * @return String[], coordinates given to place.
	 */
	private String[] placeInput(Scanner scanner, Piece piece_to_place) {
		String[] coordinates;
		String input;
		do {
			System.out.println("enter coordinates <x,y> :");
			try {
				input = scanner.next();
				coordinates = input.split(",");
				Integer.parseInt(coordinates[0]);
				Integer.parseInt(coordinates[1]);
			} catch (NumberFormatException e) {
				coordinates = null;
			}
		} while (coordinates == null || coordinates.length != 2 || Integer.parseInt(coordinates[0]) < 0
				|| Integer.parseInt(coordinates[0]) > 8 || Integer.parseInt(coordinates[1]) < 0
				|| Integer.parseInt(coordinates[1]) > 8
				|| !place(piece_to_place, Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1])));
		return coordinates;
	}

	/**
	 * Used to know what to do with the piece.
	 * 
	 * @param scanner, to get inputs.
	 * @return String, the input.
	 */
	private String moveInput(Scanner scanner) {
		String input;
		do {
			System.out.println("rotate or place :\n1: rotate  |  2: mirror  |  3: place");
			input = scanner.next();
		} while (!input.equals("1") && !input.equals("2") && !input.equals("3"));
		return input;
	}

	/**
	 * This Function check if it's possible to place a piece on the Quiltboard
	 * 
	 * @param piece, the piece to place.
	 * @param col,   the x coordinate to try placement.
	 * @param li,    the y coordinate to try placement.
	 * @return boolean, true if it's possible, false if not.
	 */
	public boolean check(Piece piece, int col, int li) {
		Objects.requireNonNull(piece, "Piece should not be null");
		if (li < 0 || li + piece.body().length > tab.length || col < 0 || col + piece.body()[0].length > tab.length) {
			return false;
		}

		for (int i = 0; i < piece.body().length; i++) {
			for (int y = 0; y < piece.body()[0].length; y++) {
				if (tab[i + li][y + col] == 1 && piece.body()[i][y] == 1) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * This function place a piece at coordinates
	 * 
	 * @param piece, the Piece to place.
	 * @param col,   the column to place the piece
	 * @param li,    the line to place the piece.
	 * @return boolean, true if it works, false if not.
	 */
	public boolean place(Piece piece, int col, int li) {
		Objects.requireNonNull(piece, "piece should not be null");
		if (check(piece, col, li)) {
			for (int i = li; i < piece.body().length + li; i++) {
				for (int y = col; y < piece.body()[0].length + col; y++) {
					if (piece.body()[i - li][y - col] == 1)
						tab[i][y] = piece.body()[i - li][y - col];
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Check if the current User has done the special Tile
	 * 
	 * @return boolean, true if he has the special Tile, false if not.
	 */
	public boolean checkSpecialTile() {
		var cmt = 0;
		for (int i = 0; i < 3; i++) {
			cmt = 0;
			for (int j = 0; j < 9; j++) {
				if (tab[i][j] == 1)
					cmt++;
				else
					cmt = 0;
				if (cmt >= 7) {
					if (checkSquare(i + 1, j - 6))
						return true;
				}
			}
		}
		return false;
	}

	/**
	 * check if there's a 6x7 piece below coordinates
	 * 
	 * @param x, the x coordinate of the starting point.
	 * @param y, the y coordinate of the starting point.
	 * @return boolean, true there's a piece of 6x7, false if not.
	 */
	public boolean checkSquare(int x, int y) {
		for (int i = x; i < x + 6; i++) {
			for (int j = y; j < y + 7; j++) {
				if (tab[i][j] == 0)
					return false;
			}
		}
		return true;
	}

	/**
	 * Count every empty cases on the User's board.
	 * 
	 * @return number of empty spaces on the board
	 */
	public int empty() {
		var cmt = 0;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (tab[i][j] == 0)
					cmt++;
			}
		}
		return cmt;
	}

	/**
	 * Basic toString method to display a QuiltBoard, 1 represent a piece placed, 0
	 * an empty space.
	 * 
	 * @return Quiltboard display (string)
	 */
	@Override
	public String toString() {
		var builder = new StringBuilder();
		var separator = "";
		for (var s : tab) {
			for (var j : s) {
				builder.append(separator).append(j);
				separator = " ";
			}
			separator = "\n";
		}
		return builder.toString();
	}

	/**
	 * Get the length of the quiltboard.
	 * 
	 * @return int, the length.
	 */
	public int length() {
		return tab.length;
	}

	/**
	 * Ckeck if the case is taken or not.
	 * 
	 * @param x, x coordinate of the case.
	 * @param y, y coordinate of the case.
	 * @return boolean, true or false if taken or not.
	 */
	public boolean isTaken(int x, int y) {
		if (x < 0 || y < 0 || x > tab.length || y > tab[0].length)
			throw new IllegalArgumentException();
		return tab[x][y] != 0;
	}
}
