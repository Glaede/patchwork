package fr.uge.patchwork;
/**
 * This class is the main.
 * 	
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 */
public class Main {
	/**
	 * main.
	 * 
	 * @param args, array of strings for the arguments.
	 */
	public static void main(String[] args) {
		Game.setup();
	}
}
