package fr.uge.patchwork;
/**
 * This interface is used to link Automa and Player.
 * 
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 */
public sealed interface User permits Player, Automa {

	/**
	 * add buttons to the user.
	 * 
	 * @param times, number of buttons to add.
	 */
	void addButtons(int times);

	/**
	 * access the quiltboard of the user.
	 * 
	 * @return QuiltBoard, the builtboard.
	 */
	default QuiltBoard board() {
		return null;
	}

	/**
	 * access the token of the user.
	 * 
	 * @return Token, the token.
	 */
	Token token();

	/**
	 * access the score of the user.
	 * 
	 * @return int, the score of the user.
	 */
	int score();

	/**
	 * acces the name of the user.
	 * 
	 * @return String, name of the user.
	 */
	String name();

	/**
	 * acces the buttons of the user.
	 * 
	 * @return int, buttons of the user.
	 */
	int buttons();

	/**
	 * add score to the user.
	 * 
	 * @param i, number of points to add to the score.
	 */
	void addScore(int i);

	/**
	 * Give the card of the user.
	 * 
	 * @return Card, the card.
	 */
	default Card card() {
		return null;
	}

	/**
	 * Used to calculate the score.
	 * 
	 * @return int, the score.
	 */
	default int calcScore() {
		return 0;
	}
}
