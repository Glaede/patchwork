package fr.uge.patchwork;
import java.util.Objects;

/**
 * This class represent a Player with a token, a quiltBoard, a certain amount of
 * buttons a score and the name of the player.
 * 
 * @author Mathieu PERRIOT
 * @author Valentin ISTASSES
 */
public final class Player implements User {

	private Token token;
	private QuiltBoard board;
	private int buttons;
	private int score;
	private String name;

	/**
	 * Constructor Player : associate the name, a quiltboard and a token to a Player
	 * and initialize his buttons and score.
	 * 
	 * @param newname, the name of the created player.
	 */
	public Player(String newname) {
		Objects.requireNonNull(newname, "Newname should not be null");
		name = newname;
		token = new Token();
		board = new QuiltBoard(9);
		buttons = 5;
		score = 0;
	}

	/**
	 * Getter to score
	 * 
	 * @return score, (int) current player's score
	 */
	public int score() {
		return score;
	}

	/**
	 * Getter to name
	 * 
	 * @return name, the name of the current player
	 */
	public String name() {
		return name;
	}

	/**
	 * Getter to the token
	 * 
	 * @return The current player's token.
	 */
	public Token token() {
		return token;
	}

	/**
	 * Getter to the QuiltBoard
	 * 
	 * @return the current players's board.
	 */
	public QuiltBoard board() {
		return board;
	}

	/**
	 * Getter to buttons.
	 * 
	 * @return buttons, (int) current player's buttons.
	 */
	public int buttons() {
		return buttons;
	}

	/**
	 * This function add the number num to the field buttons.
	 * 
	 * @param num, (int) the number of buttons to add.
	 */
	public void addButtons(int num) {
		buttons += num;
	}

	/**
	 * This function add the number num to the field score.
	 * 
	 * @param num, (int) the number of points to add.
	 */
	public void addScore(int num) {
		score += num;
	}

	/**
	 * Return the name of the current player
	 * 
	 * @return name, the String representation of the player.
	 */
	@Override
	public String toString() {
		return name;
	}
}
