package fr.uge.patchwork;
import java.util.Scanner;

/**
 * The Ascii class is mainly used to display the game in Ascii mode.
 * 
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 */
public class Ascii {

	/**
	 * Function used to print each player's turn.
	 * 
	 * @param piece_list,  list of patches.
	 * @param turn,        turn of the current game.
	 * @param timeboard,   main timeboard.
	 * @param player_turn, current player.
	 * @return String, line to print.
	 */
	public static String printTurn(CircularList piece_list, int turn, TimeBoard timeboard, User player_turn) {
		if (player_turn.getClass().equals(Automa.class))
			return "bot";
		var builder = new StringBuilder();
		builder.append("####################### TURN " + turn + " #######################").append("\n");
		builder.append(piece_list.toText(piece_list.token())).append("\n");
		;
		builder.append(timeboard).append("\n");
		builder.append("Player turn : " + player_turn.name() + " | money : " + player_turn.buttons() + " | salary : "
				+ player_turn.board().salary() + "\n");
		return builder.toString();

	}

	/**
	 * Function to represent the menu to choose what a player want to do in his
	 * turn.
	 * 
	 * @param timeboard,    the timeboard to play on.
	 * @param player_turn,  the current player.
	 * @param other_player, reresent the other player.
	 * @param scanner,      the scanner to scan the console.
	 * @param piece_list,   the list of pieces the players can buy.
	 * @param token,        describes the index of the neutral token.
	 */
	public static void turnMenu(TimeBoard timeboard, User player_turn, User other_player, Scanner scanner,
			CircularList piece_list, int token) {
		String input;
		do {
			System.out.println("\nChoose one option :\n1: move  |  2: buy");
			input = scanner.next();
		} while (!input.equals("1") && !input.equals("2"));

		if (input.equals("1")) {
			// avancer le pion
			timeboard.moveToken(player_turn, other_player, scanner);
		} else {
			// acheter une piece et la placer
			System.out.println(piece_list.next3Elements(piece_list.token()));
			buyingMenu(timeboard, player_turn, other_player, scanner, piece_list, piece_list.token());
		}
	}

	/**
	 * Function to represent the buying menu to choose piece to buy.
	 * 
	 * @param timeboard,    the timeboard to play on.
	 * @param player_turn,  the current player.
	 * @param other_player, reresent the other player.
	 * @param scanner,      the scanner to scan the console.
	 * @param piece_list,   the list of pieces the players can buy.
	 * @param token,        describes the index of the neutral token.
	 */
	public static void buyingMenu(TimeBoard timeboard, User player_turn, User other_player, Scanner scanner,
			CircularList piece_list, int token) {
		String input;
		do {
			System.out.println("Choose one piece to buy :\n 1 | 2 | 3\n back with q");
			input = scanner.next();
			if (input.equals("q")) {
				Ascii.turnMenu(timeboard, player_turn, other_player, scanner, piece_list, piece_list.token());
				return;
			}
		} while ((!input.equals("1") && !input.equals("2") && !input.equals("3"))
				|| player_turn.buttons() < piece_list.get(piece_list.token() + Integer.parseInt(input) - 1).price());

		var piece_to_place = piece_list.get(piece_list.token() + Integer.parseInt(input) - 1);
		player_turn.addButtons(-piece_list.get(piece_list.token() + Integer.parseInt(input) - 1).price());
		piece_list.deleteAt(piece_list.token() + Integer.parseInt(input) - 1);
		piece_list.moveToken(Integer.parseInt(input) - 1);

		player_turn.board().placementPhase(piece_to_place, scanner);

		timeboard.moveToken(player_turn, other_player, piece_to_place.time(), scanner);
	}
}
