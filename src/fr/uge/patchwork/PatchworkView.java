package fr.uge.patchwork;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import fr.umlv.zen5.ApplicationContext;

/**
 * This class is used to draw the graphic version.
 * 
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 */
public class PatchworkView {
	private TimeBoard timeboard;
	private User j1;
	private Color j1_color;
	private User j2;
	private Color j2_color;
	private CircularList piece_list;
	private final int width;
	private final int height;

	/**
	 * constructor of the view.
	 * 
	 * @param timeboard,  timeboard of the game.
	 * @param width,      width of the screen.
	 * @param height,     height of the screen.
	 * @param j1,         player 1.
	 * @param j2,         player 2.
	 * @param piece_list, list of pieces.
	 */
	private PatchworkView(TimeBoard timeboard, int width, int height, User j1, User j2, CircularList piece_list) {
		this.timeboard = timeboard;
		this.j1 = j1;
		this.j1_color = Color.CYAN;
		this.j2 = j2;
		this.j2_color = Color.PINK;
		this.piece_list = piece_list;
		this.width = width;
		this.height = height;
	}

	/**
	 * Initialisation of the graphic setup.
	 * 
	 * @param timeboard,  timeboard of the game.
	 * @param width,      width of the screen.
	 * @param height,     height of the screen.
	 * @param j1,         player 1.
	 * @param j2,         player 2.
	 * @param piece_list, list of pieces.
	 * @return PatchworkView, main view of the game.
	 */
	public static PatchworkView initGameGraphics(TimeBoard timeboard, int width, int height, User j1, User j2,
			CircularList piece_list) {
		return new PatchworkView(timeboard, width, height, j1, j2, piece_list);
	}

	/**
	 * Used to render each frame.
	 * 
	 * @param context, graphic context.
	 * @param view,    view of the display.
	 * @param User,    current player.
	 */
	public static void draw(ApplicationContext context, PatchworkView view, User User) {
		context.renderFrame(graphics -> view.draw(graphics, User));
	}

	/**
	 * Function to draw all of the game in one.
	 * 
	 * @param graphics, main canvas to draw on.
	 * @param User,     current player.
	 */
	private void draw(Graphics2D graphics, User User) {
		graphics.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 35));
		graphics.clearRect(0, 0, width, height);
		drawTimeBoard(graphics);
		drawQuiltBoard(graphics, User);
		drawPlayers(graphics);
		drawNext3(graphics);
		drawInterface(graphics, User);
	}

	/**
	 * Function for the placement mode.
	 * 
	 * @param context, graphic context.
	 * @param view,    view of the display.
	 * @param User,    current player.
	 * @param piece,   the piece to place.
	 * @param x,       coordinates of the piece on x.
	 * @param y,       coordinates of the piece on y.
	 */
	public static void drawPlacement(ApplicationContext context, PatchworkView view, Piece piece, int x, int y,
			User User) {
		context.renderFrame(graphics -> view.drawPlacement(graphics, piece, x, y, User));
	}

	/**
	 * Function to draw the placement phase.
	 * 
	 * @param graphics, main canvas to draw on.
	 * @param User,     current player.
	 * @param piece,    the piece to place.
	 * @param x,        coordinates of the piece on x.
	 * @param y,        coordinates of the piece on y.
	 */
	private void drawPlacement(Graphics2D graphics, Piece piece, int x, int y, User User) {
		draw(graphics, User);
		var x0 = (int) (width * 0.2);
		var y0 = (int) (height * 0.2);
		var size = (int) ((height - height * 0.2) / 9);
		drawPiece(graphics, piece, x0 + size * x, y0 + size * y, size);
	}

	/**
	 * Function to draw the interface with the statistics of the game.
	 * 
	 * @param graphics, main canvas to draw on.
	 * @param User,     current player.
	 */
	private void drawInterface(Graphics2D graphics, User User) {
		var x0 = (int) (height - height * 0.2 + width * 0.2);
		var y0 = (int) (height * 0.6);
		graphics.setColor(Color.WHITE);
		graphics.drawString("User : " + User.name(), (int) (x0 + width * 0.04), (int) (y0 + height * 0.08));
		graphics.drawString("buttons : " + User.buttons(), (int) (x0 + width * 0.04), (int) (y0 + height * 0.16));
		if (User.getClass().equals(Automa.class))
			graphics.drawString("salary : " + User.card().buttons(), (int) (x0 + width * 0.04), (int) (y0 + height * 0.24));
		else
			graphics.drawString("salary : " + User.board().salary(), (int) (x0 + width * 0.04), (int) (y0 + height * 0.24));
		graphics.drawString("LIST", (int) (x0 + width * 0.15 + width * 0.12), (int) (y0 + height * 0.08 + height * 0.08));
		graphics.drawRect((int) (x0 + width * 0.25), (int) (y0 + height * 0.1), (int) (width * 0.08), (int) (height * 0.1));
		graphics.drawString("ADVANCE", (int) (width * 0.058), (int) (height * 0.31));
		graphics.drawRect((int) (width * 0.05), (int) (height * 0.25), (int) (width * 0.1), (int) (height * 0.1));
		graphics.drawString("Place with ZQSD", width * 0.01f, height * 0.5f);
		graphics.drawString("Rotate with R", width * 0.01f, height * 0.55f);
		graphics.drawString("Mirror with T", width * 0.01f, height * 0.60f);
		graphics.drawString("Confirm with SPACE", width * 0.01f, height * 0.65f);
		graphics.drawString("Quit with Q", width * 0.01f, height * 0.75f);
	}

	/**
	 * Used to draw the list of 32 pieces on the second menu.
	 * 
	 * @param graphics, main canvas to draw on.
	 */
	void drawAllPieces(Graphics2D graphics) {
		graphics.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 35));
		var size = width / 11 < height / 4 ? width / 11 : height / 4;
		var x0 = (width - 8 * size) / 2;
		var y0 = (height - 4 * size) / 2;
		var cmt = 0;
		graphics.setColor(Color.WHITE);
		graphics.clearRect(0, 0, width, height);
		graphics.drawString("BACK", (int) (height - height * 0.2 + width * 0.2 + width * 0.15 + width * 0.12),
				(int) (height * 0.6 + height * 0.08 + height * 0.08));
		graphics.drawRect((int) (height - height * 0.2 + width * 0.2 + width * 0.25), (int) (height * 0.6 + height * 0.1),
				(int) (width * 0.08), (int) (height * 0.1));
		for (var i = 0; i < 8; i++) {
			for (var j = 0; j < 4; j++) {
				graphics.drawRect((int) (x0 + i * size), (int) (y0 + j * size), (int) size, (int) size);
				drawPiece(graphics, piece_list.get(cmt), (int) (x0 + i * size), (int) (y0 + j * size), size / 5);
				cmt++;
			}
		}
	}

	/**
	 * Function to render a frame of the scoreboard.
	 * 
	 * @param context, graphic context.
	 * @param view,    view of the display.
	 * @param j1,      player 1.
	 * @param j2,      player 2.
	 * @param width,   width of the screen.
	 * @param height,  height of the screen.
	 */
	public static void drawScoreboard(ApplicationContext context, PatchworkView view, User j1, User j2, int width,
			int height) {
		context.renderFrame(graphics -> view.drawScoreboard(graphics, j1, j2, width, height));
	}

	/**
	 * Function to draw the scoreboard.
	 * 
	 * @param graphics, main canvas to draw on.
	 * @param j1,       player 1.
	 * @param j2,       player 2.
	 * @param width,    width of the screen.
	 * @param height,   height of the screen.
	 */
	private void drawScoreboard(Graphics2D graphics, User j1, User j2, int width, int height) {
		var score1 = j1.score() + j1.buttons() - (2 * j1.board().empty());
		int score2;
		if (j2.getClass().equals(Automa.class))
			score2 = j2.calcScore();
		else
			score2 = j2.score() + j2.buttons() - (2 * j2.board().empty());
		graphics.setColor(Color.WHITE);
		graphics.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 35));
		graphics.clearRect(0, 0, width, height);
		graphics.drawString("RESULTS :", width * 0.1f, height * 0.1f);
		graphics.drawString(j1.name() + " : " + score1, width * 0.1f, height * 0.25f);
		graphics.drawString(j2.name() + " : " + score2, width * 0.1f, height * 0.35f);
		graphics.drawString("And the winner is ..... " + (score1 > score2 ? j1.name() : j2.name()), width * 0.1f,
				height * 0.5f);
	}

	/**
	 * Used to draw the next 3 pieces on the shop.
	 * 
	 * @param graphics, main canvas to draw on.
	 */
	private void drawNext3(Graphics2D graphics) {
		var x0 = height - height * 0.2 + width * 0.2;
		var y0 = height * 0.2;
		var xsize = (width - x0) / 3;
		var ysize = (height - y0) / 4;
		graphics.setColor(Color.WHITE);
		for (var i = 0; i < 3; i++) {
			drawPiece(graphics, piece_list.get(i + piece_list.token()), (int) (x0 + i * xsize), (int) y0,
					((height - height * 0.2) / 9 * 0.4));
			graphics.drawString("buttons : " + piece_list.get(i + piece_list.token()).button(),
					(int) (x0 + i * xsize + xsize * 0.1), (int) (y0 + ysize * 1.2));
			graphics.drawString("price : " + piece_list.get(i + piece_list.token()).price(),
					(int) (x0 + i * xsize + xsize * 0.1), (int) (y0 + ysize * 1.4));
			graphics.drawString("moves : " + piece_list.get(i + piece_list.token()).time(),
					(int) (x0 + i * xsize + xsize * 0.1), (int) (y0 + ysize * 1.6));
			graphics.drawRect((int) (x0 + i * xsize), (int) y0, (int) xsize, (int) ysize);
			graphics.drawRect((int) (x0 + i * xsize), (int) (y0 + ysize), (int) xsize, (int) ysize);
		}
	}

	/**
	 * Function used to draw a piece anywhere with any size.
	 * 
	 * @param graphics, main canvas to draw on.
	 * @param piece,    the piece to draw.
	 * @param x,        x coordinate.
	 * @param y,        y coordinate.
	 * @param size,     size of the piece.
	 */
	private void drawPiece(Graphics2D graphics, Piece piece, int x, int y, double size) {
		if (piece == null)
			return;

		for (var i = 0; i < piece.body().length; i++) {
			for (var j = 0; j < piece.body()[0].length; j++) {
				if (piece.body()[i][j] == 1) {
					graphics.setColor(Color.ORANGE);
					graphics.fillRect((int) (x + i * size), (int) (y + j * size), (int) size, (int) size);
					graphics.setColor(Color.WHITE);
					graphics.drawRect((int) (x + i * size), (int) (y + j * size), (int) size, (int) size);
				}
			}
		}
	}

	/**
	 * Function to draw players on the screen.
	 * 
	 * @param graphics, main canvas to draw on.
	 */
	private void drawPlayers(Graphics2D graphics) {
		var size = (int) (height * 0.2);
		var start = j1.token().place() < j2.token().place() ? j1.token().place() : j2.token().place();

		graphics.setColor(j1_color);
		graphics.drawString(j1.name().charAt(0) + "", (j1.token().place() - start) * size + size / 2 - size / 10,
				(int) (size * 0.25) - size / 10);
		graphics.fillOval((j1.token().place() - start) * size + size / 2 - size / 10, (int) (size * 0.25) - size / 10,
				size / 5, size / 5);

		graphics.setColor(j2_color);
		graphics.drawString(j2.name().charAt(0) + "", (j2.token().place() - start) * size + size / 2 - size / 10,
				(int) (size * 0.75) - size / 10);
		graphics.fillOval((j2.token().place() - start) * size + size / 2 - size / 10, (int) (size * 0.75) - size / 10,
				size / 5, size / 5);
	}

	/**
	 * Used to display the Quildboard of the current player.
	 * 
	 * @param graphics, main canvas to draw on.
	 * @param user,     the current player.
	 */
	private void drawQuiltBoard(Graphics2D graphics, User user) {
		if (user.getClass().equals(Automa.class))
			return;
		var x0 = width * 0.2;
		var y0 = height * 0.2;
		var size = (int) ((height - height * 0.2) / 9);
		graphics.setColor(Color.WHITE);
		for (var i = 0; i < user.board().length(); i++) {
			int x_place = (int) (x0 + size * i);
			for (var j = 0; j < user.board().length(); j++) {
				if (user.board().isTaken(i, j)) {
					graphics.setColor(Color.RED);
					graphics.fillRect(x_place, (int) y0 + size * j, size, size);
				} else {
					graphics.setColor(Color.WHITE);
					graphics.drawRect(x_place, (int) y0 + size * j, size, size);
				}
			}
		}
	}

	/**
	 * Function used to draw the timeboard.
	 * 
	 * @param graphics, main canvas to draw on.
	 */
	private void drawTimeBoard(Graphics2D graphics) {
		var size = (int) (height * 0.2);
		var start = j1.token().place() < j2.token().place() ? j1.token().place() : j2.token().place();

		for (var i = 0; i * size < width; i++) {
			graphics.setColor(Color.WHITE);
			graphics.drawRect(i * size, 0, size, size);

			if (start + i > timeboard.plateau.length - 1)
				return;

			if (timeboard.plateau[start + i].button()) {
				graphics.setColor(Color.BLUE);
				graphics.fillOval(i * size - size / 10, size / 2 - size / 10, size / 5, size / 5);
			}
			if (timeboard.plateau[start + i].patch()) {
				graphics.setColor(Color.RED);
				graphics.fillRect(i * size - size / 10, size / 2 - size / 10, size / 5, size / 5);
			}
		}
	}
}
