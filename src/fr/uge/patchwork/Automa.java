package fr.uge.patchwork;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 * This class is the Automa used to play against a bot.
 * 
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 */
public final class Automa implements User {
	private static int deck_type;
	private static Token token;
	private static int difficulty;
	private static int buttons;
	private static int score;
	private static ArrayList<Card> deck;
	private static ArrayList<Card> discard;
	private static Card card;
	private static int patches_with_buttons;
	private static int buttons_on_patches;

	/**
	 * Contructor for an Automa.
	 * 
	 * @param _difficulty, difficulty of the bot.
	 * @param _deck_type,  tactical or normal deck.
	 */
	public Automa(int _difficulty, int _deck_type) {
		deck = new ArrayList<Card>();
		discard = new ArrayList<Card>();
		token = new Token();
		difficulty = _difficulty;
		deck_type = _deck_type;
		buttons = 0;
		score = 0;
		buttons_on_patches = 0;
		patches_with_buttons = 0;
	}

	/**
	 * Main function for the bot's turn.
	 * 
	 * @param timeboard,    the main board.
	 * @param piece_list,   list of patches.
	 * @param bot,          the Automa player.
	 * @param player,       the player.
	 * @param special_tile, value if the special tile is in game.
	 * @return boolean, new value of the special tile.
	 */
	public static boolean botTurn(TimeBoard timeboard, CircularList piece_list, User bot, User player,
			boolean special_tile) {
		if (deck.size() == 0)
			newDeck();
		card = nextCard();

		if (special_tile && bot.token().place() >= Automa.toReach()) {
			bot.addScore(7);
			special_tile = false;
		}
		var pieces = new ArrayList<Piece>();
		for (var i = piece_list.token(); i < 3 + piece_list.token(); i++)
			pieces.add(piece_list.get(i));
		pieces = checkPrice(pieces);
		Piece piece = null;

		if (pieces.size() == 0)
			timeboard.moveToken(bot, player, 0, null);
		else if (pieces.size() == 1) {
			piece = pieces.get(0);
			piece_list.delete(piece);
		} else
			piece = applyFilters(pieces, bot, player);

		if (piece == null)
			timeboard.moveToken(bot, player, 0, null);
		else
			takePiece(piece, piece_list, timeboard, bot, player);
		return special_tile;
	}

	/**
	 * Function if the bot buy a piece.
	 * 
	 * @param piece,      piece to buy.
	 * @param piece_list, list of pieces.
	 * @param timeboard,  main timeboard.
	 * @param bot,        bot player.
	 * @param player,     real player.
	 */
	private static void takePiece(Piece piece, CircularList piece_list, TimeBoard timeboard, User bot, User player) {
		timeboard.moveToken(bot, player, piece.time(), null);
		piece_list.moveToken(piece_list.getIndex(piece) - piece_list.token());
		piece_list.delete(piece);
		if (piece.button() > 0) {
			patches_with_buttons++;
			buttons_on_patches += piece.button();
		}
	}

	/**
	 * Function to apply filters from card.
	 * 
	 * @param pieces, list of three pieces from shop.
	 * @param bot,    bot player.
	 * @param player, real player.
	 * @return Piece, the piece to buy.
	 */
	private static Piece applyFilters(ArrayList<Piece> pieces, User bot, User player) {
		for (var i = 0; i < card.filters().size(); i++) {
			if (card.filters().get(i) != null) {
				pieces = applyFilter(pieces, card.filters().get(i), bot, player);
			}
			if (pieces == null || pieces.size() == 0)
				return null;
			if (pieces.size() == 1)
				return pieces.get(0);
		}
		return null;
	}

	/**
	 * Function who receive a filter and apply it with the correct function.
	 * 
	 * @param pieces, list of pieces to filter.
	 * @param filter, the filter to apply.
	 * @param bot,    bot player.
	 * @param player, real player.
	 * @return ArrayList<Piece>, the list of filtered pieces.
	 */
	private static ArrayList<Piece> applyFilter(ArrayList<Piece> pieces, Filter filter, User bot, User player) {
		return switch (filter.type()) {
		case 1 -> Filter.minMoves(pieces, bot, player);
		case 2 -> Filter.largest(pieces, bot, player);
		case 3 -> Filter.mostButtons(pieces);
		case 4 -> Filter.furthest(pieces);
		default -> null;
		};
	}

	/**
	 * check if the bot can buy at least one piece.
	 * 
	 * @param pieces, list of pieces.
	 * @return ArrayList<Piece>, list of pieces the bot can buy.
	 */
	private static ArrayList<Piece> checkPrice(ArrayList<Piece> pieces) {
		for (var i = 0; i < pieces.size(); i++) {
			if (pieces.get(i).price() > card.budget()) {
				pieces.remove(pieces.get(i));
				i--;
			}
		}
		return pieces;
	}

	/**
	 * Function to shuffle the discard list and put it in the deck of the bot.
	 */
	private static void newDeck() {
		for (var card : discard) {
			deck.add(card);
		}
		discard.clear();
		shuffle();
	}

	/**
	 * Function to load the deck for the bot.
	 * 
	 * @param file, the path of the file
	 * @throws IOException if the file doesn't exist.
	 */
	public static void loadDeck(String file) throws IOException {
		var path = Path.of(file);
		var del = deck_type == 1 ? 0 : 12;
		try (var reader = Files.newBufferedReader(path)) {
			for (var i = 0 + del; i < 12 + del; i++) {
				var line = reader.readLine();
				var items = line.split("/");
				var filters = new ArrayList<Filter>();
				filters.add(new Filter(Integer.parseInt(items[2])));
				filters.add(new Filter(Integer.parseInt(items[3])));
				filters.add(new Filter(Integer.parseInt(items[4])));
				deck.add(new Card(filters, Integer.parseInt(items[0]), Integer.parseInt(items[1])));
			}
		}
		shuffle();
	}

	/**
	 * Function to shuffle the bot's deck.
	 */
	private static void shuffle() {
		for (int i = 0; i < 150; i++) {
			var n1 = (int) (Math.random() * 12);
			var n2 = (int) (Math.random() * 12);
			var tmp_piece = deck.get(n1);
			deck.add(n1, deck.get(n2));
			deck.remove(n1);
			deck.add(n2, tmp_piece);
			deck.remove(n2);
		}
	}

	/**
	 * Function to get the next card of the deck.
	 * 
	 * @return Card, the next card.
	 */
	private static Card nextCard() {
		discard.add(deck.get(0));
		deck.remove(0);
		discard.get(discard.size() - 1);
		return discard.get(discard.size() - 1);
	}

	/**
	 * Function to calculate the bot's score at the end.
	 */
	public int calcScore() {
		return switch (difficulty) {
		case 1 -> score;
		case 2 -> score + buttons;
		case 3 -> score + buttons + patches_with_buttons;
		case 4 -> score + buttons + buttons_on_patches;
		case 5 -> score + buttons + patches_with_buttons + buttons_on_patches;
		default -> throw new IllegalArgumentException("Unexpected difficulty: " + difficulty);
		};
	}

	/**
	 * Function to get the case to reach for the bot to get the special tile.
	 * 
	 * @return int, the case to reach.
	 */
	public static int toReach() {
		return switch (difficulty) {
		case 1 -> 51;
		case 2 -> 43;
		case 3 -> 40;
		case 4 -> 37;
		case 5 -> 34;
		default -> throw new IllegalArgumentException("Unexpected difficulty: " + difficulty);
		};
	}

	/**
	 * returns the card of the bot.
	 */
	public Card card() {
		return card;
	}

	@Override
	/**
	 * add times to the bot's buttons.
	 */
	public void addButtons(int times) {
		buttons += times;
	}

	@Override
	public Token token() {
		return token;
	}

	@Override
	public int score() {
		return score;
	}

	@Override
	public String name() {
		return "automa";
	}

	@Override
	public int buttons() {
		return buttons;
	}

	@Override
	/**
	 * add i to the bot's score.
	 */
	public void addScore(int i) {
		score += i;
	}
}
