package fr.uge.patchwork;
import java.util.ArrayList;

/**
 * This class is used to filter with the cards of the Automa.
 * 
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 * 
 * @param type, the type of the filter.
 */
public record Filter(int type) {

	/**
	 * Used to filter a list to get the pieces with the minimum moves in.
	 * 
	 * @param piece_list, list of pieces to filter.
	 * @param j1,         real player.
	 * @param user,       the bot.
	 * @return ArrayList, the filtered list.
	 */
	public static ArrayList<Piece> minMoves(ArrayList<Piece> piece_list, User j1, User user) {
		var res = new ArrayList<Piece>();
		for (var piece : piece_list) {
			if (piece.time() + user.token().place() < j1.token().place())
				res.add(piece);
		}
		if (res.size() == 0)
			return piece_list;
		return res;
	}

	/**
	 * Used to filter a list to get the biggest pieces.
	 * 
	 * @param piece_list, list of pieces to filter.
	 * @param j1,         real player.
	 * @param user,       the bot.
	 * @return ArrayList, the filtered list.
	 */
	public static ArrayList<Piece> largest(ArrayList<Piece> piece_list, User j1, User user) {
		var res = new ArrayList<Piece>();
		var max_size = piece_list.get(0).length();
		res.add(piece_list.get(0));
		for (var i = 1; i < piece_list.size(); i++) {
			if (piece_list.get(i).length() == max_size)
				res.add(piece_list.get(i));
			else if (piece_list.get(i).length() >= max_size) {
				res.clear();
				res.add(piece_list.get(i));
			}
		}
		if (res.size() == 0)
			return piece_list;
		return res;
	}

	/**
	 * Used to filter a list to get the pieces with the most buttons on it.
	 * 
	 * @param piece_list, list of pieces to filter.
	 * @return ArrayList, the filtered list.
	 */
	public static ArrayList<Piece> mostButtons(ArrayList<Piece> piece_list) {
		var res = new ArrayList<Piece>();
		var max_buttons = piece_list.get(0).button();
		res.add(piece_list.get(0));
		for (var i = 1; i < piece_list.size(); i++) {
			if (piece_list.get(i).button() == max_buttons)
				res.add(piece_list.get(i));
			else if (piece_list.get(i).button() >= max_buttons) {
				res.clear();
				res.add(piece_list.get(i));
			}
		}
		if (res.size() == 0)
			return piece_list;
		return res;
	}

	/**
	 * Used to filter a list to get the furthest piece in.
	 * 
	 * @param piece_list, list of pieces to filter.
	 * @return ArrayList, the filtered list.
	 */
	public static ArrayList<Piece> furthest(ArrayList<Piece> piece_list) {
		var res = new ArrayList<Piece>();
		res.add(piece_list.get(piece_list.size() - 1));
		return res;
	}
}
