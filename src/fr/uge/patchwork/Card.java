package fr.uge.patchwork;
import java.util.ArrayList;

/**
 * This record is the representation of a Card.
 * 
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 * 
 * @param filters, list of filters.
 * @param budget,  budget of the card.
 * @param buttons, buttons on the card.
 */
public record Card(ArrayList<Filter> filters, int budget, int buttons) {
}
