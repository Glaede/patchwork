package fr.uge.patchwork;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

/**
 * Represent a TimeBoard which is composed of 54 Case.
 * 
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 */
public class TimeBoard {

	final Case[] plateau;

	/**
	 * Create a TimeBoard which is composed of 54 Case.
	 */
	public TimeBoard() {
		plateau = new Case[54];
	}

	/**
	 * Load a board using a text file. This method use a certain type of file you
	 * first have to indicate the number of button, then type "/" and indicate the
	 * number of patch on a case
	 * 
	 * @param j1    which is the current User to add to the case 0
	 * @param j2    which is the other User to add to the case 0
	 * @param file, the path of the file.
	 * @throws IOException if the file is not found.
	 */
	public void loadBoard(User j1, User j2, String file) throws IOException {
		Objects.requireNonNull(j1, "j1 should not be null");
		Objects.requireNonNull(j2, "j2 should not be null");

		var path = Path.of(file);
		try (var reader = Files.newBufferedReader(path)) {
			String line;
			var players = new ArrayList<User>();
			players.add(0, j1);
			players.add(0, j2);
			var i = 1;
			plateau[0] = new Case(false, false, players, 0);
			while ((line = reader.readLine()) != null) {
				var items = line.split("/");
				var button = (Integer.parseInt(items[0]) == 1);
				var patch = (Integer.parseInt(items[1]) == 1);
				plateau[i] = new Case(button, patch, new ArrayList<User>(), i);
				i++;
			}
		}
	}

	/**
	 * This function returns the current User.
	 * 
	 * @return a User, the current User.
	 */
	public User playerTurn() {
		for (int i = 0; i < 54; i++) {
			if (plateau[i].players() != null) {
				if (plateau[i].players().size() > 0)
					return plateau[i].players().get(0);
			}
		}
		return null;
	}

	/**
	 * Give the other player to the main program.
	 * 
	 * @param player_turn, the current player.
	 * @param j1,          player 1.
	 * @param j2,          player 2.
	 * @return User, the other player.
	 */
	public User other(User player_turn, User j1, User j2) {
		if (player_turn.equals(j1))
			return j2;
		else
			return j1;
	}

	/**
	 * This function move the current User's token on the timeboard in front of the
	 * other User's token.
	 * 
	 * @param j1,      the current User.
	 * @param j2,      the other User.
	 * @param scanner, a text scanner.
	 */
	public void moveToken(User j1, User j2, Scanner scanner) {
		moveToken(j1, j2, 0, scanner);
	}

	/**
	 * This function move the current User's token on the timeboard a certain number
	 * of moves.
	 * 
	 * @param j1,      the current User.
	 * @param j2,      the other User.
	 * @param moves,   (int) move step by step the current User's token moves times.
	 * @param scanner, a text scanner.
	 */
	public void moveToken(User j1, User j2, int moves, Scanner scanner) {
		Objects.requireNonNull(j1, "j1 should not be null");
		Objects.requireNonNull(j2, "j2 should not be null");

		if (moves < 0)
			throw new IllegalArgumentException(); // verif
		int times;
		if (moves > 0)
			times = moves;
		else {
			times = 1 + j2.token().place() - j1.token().place();
			if (j1.getClass().equals(Player.class))
				j1.addButtons(times);
		}
		if (times + j1.token().place() > 53)
			times = 53 - j1.token().place(); // verif
		for (int i = 0; i < times; i++) {
			if (plateau[j1.token().place() + 1].button())
				button(j1);
			else if (plateau[j1.token().place() + 1].patch())
				patch(j1, scanner);

			plateau[j1.token().place() + 1].players().add(0, plateau[j1.token().place()].players().get(0));
			plateau[j1.token().place()].players().remove(0);
			j1.token().move(1);
		}
	}

	/**
	 * Give the salary to the user.
	 * 
	 * @param j1, the player.
	 */
	private void button(User j1) {
		if (j1.getClass().equals(Automa.class))
			j1.addButtons(j1.card().buttons());
		else
			j1.addButtons(j1.board().salary());
	}

	/**
	 * Used to act when a patch is on the path.
	 * 
	 * @param j1,      the current player.
	 * @param scanner, scanner to get inputs.
	 */
	private void patch(User j1, Scanner scanner) {
		plateau[j1.token().place() + 1] = plateau[j1.token().place() + 1].removePatch();
		if (scanner != null) {
			var body = new int[1][1];
			body[0][0] = 1;
			j1.board().placementPhase(new Piece(0, 0, 0, body), scanner);
		} else if (j1.getClass().equals(Player.class))
			GameController.patch = true;
	}

	/**
	 * This method display the victory screen using a StringBuilder.
	 * 
	 * @param j1, User 1.
	 * @param j2, User 2.
	 * @return String,
	 */
	public String victory(User j1, User j2) {
		var builder = new StringBuilder();
		User winner;

		var score1 = j1.score() + j1.buttons() - (2 * j1.board().empty());
		var score2 = j2.score() + j2.buttons() - (2 * j2.board().empty());
		builder.append("\n### SCOREBOARD ###\n");
		if (score1 > score2) {
			winner = j1;
			builder.append(j1.name()).append(" : ").append(score1).append("\n");
			builder.append(j2.name()).append(" : ").append(score2).append("\n");
		} else if (score2 > score1) {
			winner = j2;
			builder.append(j1.name()).append(" : ").append(score1).append("\n");
			builder.append(j2.name()).append(" : ").append(score2).append("\n");
		} else {
			winner = plateau[53].players().get(1);
			builder.append(j1.name()).append(" : ").append(score1).append("\n");
			builder.append(j2.name()).append(" : ").append(score2).append("\n");
		}
		builder.append("\nAnd the winner is ..... ").append(winner.name()).append(" !!!");
		return builder.toString();
	}

	/**
	 * This method display a timeboard and its buttons/patchs with User token on it
	 * using a StringBuilder.
	 * 
	 * @return String,
	 */
	@Override
	public String toString() {
		var builder = new StringBuilder();
		for (var i = 0; i < 54; i++)
			builder.append("====");
		builder.append("\n");
		for (var i = 0; i < 54; i++) {
			if (plateau[i].players().size() > 0)
				builder.append("| ").append(plateau[i].players().get(0).name().charAt(0)).append(" ");
			else
				builder.append("|   ");
		}
		builder.append("\n");
		for (var i = 0; i < 54; i++) {
			if (plateau[i].patch())
				builder.append("#   ");
			else if (plateau[i].button())
				builder.append("O   ");
			else
				builder.append("|   ");
		}
		builder.append("\n");
		for (var i = 0; i < 54; i++) {
			if (plateau[i].players().size() > 1)
				builder.append("| ").append(plateau[i].players().get(1).name().charAt(0)).append(" ");
			else
				builder.append("|   ");
		}
		builder.append("\n");
		for (var i = 0; i < 54; i++)
			builder.append("====");
		return builder.toString();
	}
}
