package fr.uge.patchwork;
import java.util.Objects;
import java.util.ArrayList;

/**
 * This record create a Case.
 * 
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 * 
 * @param button,  if the case has a button on it
 * @param patch,   if the patch has a patch on it
 * @param players, the User list
 * @param place,   the place of the case.
 */
public record Case(boolean button, boolean patch, ArrayList<User> players, int place) {

	/**
	 * This constructor initialize a Case.
	 */
	public Case {
		Objects.requireNonNull(players, "User list should not be null");
	}

	/**
	 * This method remove a patch in a case.
	 * 
	 * @return a new case with no patch on it.
	 */
	public Case removePatch() {
		return new Case(button, false, players, place);
	}
}
