package fr.uge.patchwork;
import java.awt.Color;
import java.io.IOException;
import java.util.Scanner;

import fr.umlv.zen5.Application;

/**
 * This class is the main game.
 * 
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 */
public class Game {

	/**
	 * Function to initialize the game.
	 */
	public static void setup() {
		// initialisation des elements de jeu
		var timeboard = new TimeBoard();
		var piece_list = new CircularList();
		var scanner = new Scanner(System.in);
		var version = versionInput(scanner);
		var graphic = graphicInput(scanner);

		System.out.println("Name User 1 :");
		var j1 = new Player(scanner.next());
		var j2 = dualSetup(version, piece_list, timeboard, scanner, j1);

		if (graphic == 1) {
			scanner.close();
			Application.run(new Color(66, 69, 72),
					context -> GameController.PatchworkGame(context, timeboard, j1, j2, piece_list));
		} else {
			game(timeboard, j1, j2, scanner, piece_list, version == 2, graphic);
			scanner.close();
			System.out.println(timeboard.victory(j1, j2));
		}
	}

	/**
	 * Use a scanner to get the version.
	 * 
	 * @param scanner, scanner to get input.
	 * @return int, the version selected.
	 */
	private static int versionInput(Scanner scanner) {
		String input;
		do {
			System.out.println("Which version to play on :\n1 : demo  |  2 : complete");
			input = scanner.next();
		} while (!input.equals("1") && !input.equals("2"));
		return Integer.parseInt(input);
	}

	/**
	 * Use a scanner to get the display mode.
	 * 
	 * @param scanner, scanner to get input.
	 * @return int, graphic mode or not.
	 */
	private static int graphicInput(Scanner scanner) {
		String input;
		do {
			System.out.println("Graphic or ASCII mode :\n1 : Graphic  |  2 : ASCII");
			input = scanner.next();
		} while (!input.equals("1") && !input.equals("2"));
		return Integer.parseInt(input);
	}

	/**
	 * Function to setup the game if you play with a human.
	 * 
	 * @param version,    version of the game.
	 * @param piece_list, list of pieces.
	 * @param timeboard,  board to play.
	 * @param scanner,    scanner to get inputs.
	 * @param j1,         player one.
	 * @return User, a Player 2.
	 */
	private static User dualSetup(int version, CircularList piece_list, TimeBoard timeboard, Scanner scanner, User j1) {
		String input;
		do {
			System.out.println("Who is your opponent ?\n1 : Automa  |  2 : J2");
			input = scanner.next();
		} while (!input.equals("1") && !input.equals("2"));
		var automa = Integer.parseInt(input);

		User j2;
		if (automa == 1)
			j2 = botSetup(version, piece_list, timeboard, scanner, j1);
		else {
			System.out.println("Name User 2 :");
			j2 = new Player(scanner.next());
		}
		loadFiles(version, piece_list, timeboard, j1, j2);
		return j2;
	}

	/**
	 * Function to setup the game if you play against the Automa.
	 * 
	 * @param version,    version of the game.
	 * @param piece_list, list of pieces.
	 * @param timeboard,  board to play.
	 * @param scanner,    scanner to get inputs.
	 * @param j1,         player one.
	 * @return User, an Automa.
	 */
	private static User botSetup(int version, CircularList piece_list, TimeBoard timeboard, Scanner scanner, User j1) {
		String input;
		do {
			System.out
					.println("Which difficulty ?\n1 : Intern  |  2 : Apprentice  |  3 : Fellow  |  4 : Master  |  5 : Legend");
			input = scanner.next();
		} while (!input.equals("1") && !input.equals("2") && !input.equals("3") && !input.equals("4")
				&& !input.equals("5"));
		var difficulty = Integer.parseInt(input);

		do {
			System.out.println("Which deck ?\n1 : Normal  |  2 : Tactical");
			input = scanner.next();
		} while (!input.equals("1") && !input.equals("2"));
		var deck_type = Integer.parseInt(input);
		return new Automa(difficulty, deck_type);
	}

	/**
	 * Function used to load informations from files.
	 * 
	 * @param version,    version of the game.
	 * @param piece_list, list of pieces.
	 * @param timeboard,  board to play.
	 * @param j1,         player 1.
	 * @param j2,         player 2.
	 */
	private static void loadFiles(int version, CircularList piece_list, TimeBoard timeboard, User j1, User j2) {
		// load des fichiers de pieces et de timeboard
		try {
			if (version == 1)
				piece_list.loadList("src/piecesv1");
			else
				piece_list.loadList("src/piecesv2");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			if (version == 1)
				timeboard.loadBoard(j1, j2, "src/timeboardv1");
			else
				timeboard.loadBoard(j1, j2, "src/timeboardv2");
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (j2.getClass().equals(Automa.class)) {
			try {
				Automa.loadDeck("src/cards");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Function to follow the progress of the game.
	 * 
	 * @param timeboard,    the timeboard to play on.
	 * @param j1,           the User 1.
	 * @param j2,           the User 2.
	 * @param scanner,      the scanner to scan the console.
	 * @param piece_list,   the list of pieces the players can buy.
	 * @param special_tile, true if the special tile is in the game.
	 * @param graphic,      which graphic mode are we using.
	 */
	public static void game(TimeBoard timeboard, User j1, User j2, Scanner scanner, CircularList piece_list,
			boolean special_tile, int graphic) {
		var turn = 1;
		while (timeboard.plateau[53].players().size() != 2) {

			var player_turn = timeboard.playerTurn();
			var other_player = timeboard.other(player_turn, j1, j2);

			if (player_turn.getClass().equals(Automa.class)) {
				if (special_tile && player_turn.token().place() >= Automa.toReach()) {
					player_turn.addScore(7);
					special_tile = false;
				}
				Automa.botTurn(timeboard, piece_list, player_turn, other_player, special_tile);
			} else {
				if (special_tile && player_turn.getClass().equals(Player.class)
						&& (special_tile = player_turn.board().checkSpecialTile())) {
					player_turn.addScore(7);
					special_tile = false;
				}
				if (graphic == 2)
					System.out.println(Ascii.printTurn(piece_list, turn, timeboard, player_turn));
				Ascii.turnMenu(timeboard, player_turn, other_player, scanner, piece_list, piece_list.token());
			}
			turn++;
		}
	}
}
