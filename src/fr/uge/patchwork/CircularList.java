package fr.uge.patchwork;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Represent a Circularlist (which is an ArrayList) of piece.
 * 
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 */
public class CircularList {
	private final ArrayList<Piece> pieces;
	private int token;

	/**
	 * Constructor of a Circularlist (which is an ArrayList) of piece.
	 */
	public CircularList() {
		pieces = new ArrayList<Piece>();
	}

	/**
	 * This method return a Piece at index.
	 * 
	 * @param index, (int) the index in the list of the circularList
	 * @return a piece if the index is valid, null if not.
	 */
	public Piece get(int index) {
		if (pieces != null && pieces.size() > index)
			return pieces.get(index);
		return null;
	}

	/**
	 * This method load a list of piece in the circularList using a text file and
	 * shuffle it. 1 reprensent a piece, 0 an empty space, ',' represent a newline
	 * and then you can precise the price, the number of time and the number of
	 * button.
	 * 
	 * @param file, the path of the file to open.
	 * @throws IOException if the file is not found.
	 */
	public void loadList(String file) throws IOException {
		var path = Path.of(file);
		try (var reader = Files.newBufferedReader(path)) {
			String line;

			while ((line = reader.readLine()) != null) {
				var item = line.split("/");
				var body_str = item[0].split(",");

				int[][] body = new int[body_str.length][body_str[0].length()];
				for (var i = 0; i < body_str.length; i++) {
					for (var j = 0; j < body_str[i].length(); j++) {
						body[i][j] = body_str[i].charAt(j) - 48;
					}
				}
				pieces.add(new Piece(Integer.parseInt(item[1]), Integer.parseInt(item[2]), Integer.parseInt(item[3]), body));
			}
			shuffle();
		}
	}

	/**
	 * this function add a piece to the list
	 * 
	 * @param p, piece to add in the list.
	 */
	public void add(Piece p) {
		Objects.requireNonNull(p);
		pieces.add(p);
	}

	/**
	 * this function delete a piece in the list
	 * 
	 * @param p, piece to delete from the list.
	 */
	public void delete(Piece p) {
		Objects.requireNonNull(p);
		pieces.remove(p);
	}

	/**
	 * this function delete a piece at the index precised
	 * 
	 * @param index, (int) the index in a list
	 */
	public void deleteAt(int index) {
		if (index > pieces.size() || index < 0)
			throw new IllegalArgumentException();
		pieces.remove(index);
	}

	/**
	 * this function shuffle pieces in the circularList.
	 */
	void shuffle() {
		for (int i = 0; i < 150; i++) {
			var n1 = (int) (Math.random() * 32);
			var n2 = (int) (Math.random() * 32);
			var tmp_piece = pieces.get(n1);
			pieces.add(n1, pieces.get(n2));
			pieces.remove(n1 + 1);
			pieces.add(n2, tmp_piece);
			pieces.remove(n2 + 1);
		}
	}

	/**
	 * this function display the next 3 elements from index.
	 * 
	 * @param index, the index where it starts to display the 3 pieces.
	 * @return string to display the 3 pieces.
	 */
	public String next3Elements(int index) {
		var builder = new StringBuilder();
		if (index > pieces.size() || index < 0)
			throw new IllegalArgumentException();
		if (pieces.size() < 3) {
			for (var s : pieces)
				builder.append(s.toString());
		} else {
			for (int i = 0; i < 3; i++) {
				builder.append(pieces.get(index % pieces.size()));
				index++;
			}
		}
		return builder.toString();
	}

	/**
	 * String representation of the list.
	 * 
	 * @param index, index to where the neutral token is.
	 * @return string to display.
	 */
	public String toText(int index) {
		var builder = new StringBuilder();
		var cmt = 0;
		for (var j = 0; j < 4; j++) {
			builder.append("\n");
			cmt = 0;
			for (var piece : pieces) {
				if (cmt == index)
					builder.append("O ");
				else
					builder.append("| ");
				cmt++;
				if (piece.body().length > j) {
					for (var i = 0; i < 6; i++) {
						if (piece.body()[0].length > i) {
							if (piece.body()[j][i] == 1) {
								builder.append("X");
							} else {
								builder.append(" ");
							}
						} else {
							builder.append(" ");
						}
					}
				} else {
					for (var i = 0; i < 6; i++) {
						builder.append(" ");
					}
				}
			}
		}
		builder.append("\n");
		return builder.toString();
	}

	/**
	 * getter for the neutral token.
	 * 
	 * @return int, the place of the token.
	 */
	public int token() {
		return token;
	}

	/**
	 * Function to move the token.
	 * 
	 * @param i, times you want hime to move.
	 */
	public void moveToken(int i) {
		token += i;
	}

	/**
	 * Get the length of the circular list.
	 * 
	 * @return int, size of the list.
	 */
	public int length() {
		return pieces.size();
	}

	/**
	 * Get the index of a piece in the list.
	 * 
	 * @param piece, the piece you want to find.
	 * @return int, the index of the piece.
	 */
	public int getIndex(Piece piece) {
		var i = 0;
		for (var elem : pieces) {
			if (elem.equals(piece))
				return i;
			i++;
		}
		return 0;
	}
}
