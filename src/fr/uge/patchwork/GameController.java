package fr.uge.patchwork;
import java.awt.geom.Point2D;

import fr.umlv.zen5.ApplicationContext;
import fr.umlv.zen5.Event;
import fr.umlv.zen5.ScreenInfo;
import fr.umlv.zen5.Event.Action;
import fr.umlv.zen5.KeyboardKey;

/**
 * This class is used to control the graphic version.
 * 
 * @author Mathieu PERRIOT.
 * @author Valentin ISTASSES.
 */
public class GameController {
	static boolean patch = false;

	/**
	 * Main structure of the graphic game.
	 * 
	 * @param context,    graphic context.
	 * @param timeboard,  timeboard to play.
	 * @param j1,         player 1.
	 * @param j2,         player 2.
	 * @param piece_list, list of pieces.
	 */
	static void PatchworkGame(ApplicationContext context, TimeBoard timeboard, User j1, User j2,
			CircularList piece_list) {
		// get the size of the screen
		ScreenInfo screenInfo = context.getScreenInfo();
		int width = (int) screenInfo.getWidth();
		int height = (int) screenInfo.getHeight();
		int list_menu = 0;
		boolean special_tile = true;

		PatchworkView view = PatchworkView.initGameGraphics(timeboard, width, height, j1, j2, piece_list);

		globalGame(context, view, timeboard, j1, j2, special_tile, piece_list, list_menu, width, height);

		PatchworkView.drawScoreboard(context, view, j1, j2, width, height);
		Event event;
		do {
			event = context.pollEvent();
		} while (event == null || event.getAction() != Action.KEY_PRESSED);
		context.exit(1);
	}

	/**
	 * Main loop of the graphic version.
	 * 
	 * @param context,      graphic context.
	 * @param view,         view of the display.
	 * @param timeboard,    timeboard to play.
	 * @param j1,           player 1.
	 * @param j2,           player 2.
	 * @param piece_list,   list of pieces.
	 * @param special_tile, if the special tile is available.
	 * @param list_menu,    if we are in the list display.
	 * @param width,        width of the screen.
	 * @param height,       height of the screen.
	 */
	private static void globalGame(ApplicationContext context, PatchworkView view, TimeBoard timeboard, User j1, User j2,
			boolean special_tile, CircularList piece_list, int list_menu, int width, int height) {
		while (timeboard.plateau[53].players().size() != 2) {

			var player_turn = timeboard.playerTurn();
			var other_player = timeboard.other(player_turn, j1, j2);

			if (player_turn.getClass().equals(Automa.class))
				special_tile = Automa.botTurn(timeboard, piece_list, player_turn, other_player, special_tile);
			else {
				Event event = context.pollOrWaitEvent(10);
				Point2D.Float mouse = null;

				if (event == null)
					continue;

				Action action = event.getAction();
				KeyboardKey key = event.getKey();
				if (action == Action.KEY_PRESSED && key.toString().equals("Q"))
					context.exit(1);
				else if (action == Action.POINTER_DOWN) {
					list_menu = action(context, view, mouse, event, width, height, list_menu, player_turn, other_player,
							piece_list, timeboard);
				}
				if (patch)
					patch = placePatch(context, view, player_turn);
			}
			if (special_tile && player_turn.getClass().equals(Player.class) && (player_turn.board().checkSpecialTile())) {
				player_turn.addScore(7);
				special_tile = false;
			}
			if (list_menu == 0)
				PatchworkView.draw(context, view, player_turn);
		}
	}

	/**
	 * Function to place a simple 1 by 1 patch.
	 * 
	 * @param context,     graphic context.
	 * @param view,        view of the display.
	 * @param player_turn, player who is playing.
	 * @return boolean, false because there is no more patches.
	 */
	private static boolean placePatch(ApplicationContext context, PatchworkView view, User player_turn) {
		var body = new int[1][1];
		body[0][0] = 1;
		placement(context, view, new Piece(0, 0, 0, body), player_turn);
		return false;
	}

	/**
	 * Function who translate the Event in an action.
	 * 
	 * @param mouse,        current point of where the mouse is.
	 * @param event,        given event.
	 * @param player_turn,  player who is playing.
	 * @param other_player, the other player.
	 * @param context,      graphic context.
	 * @param view,         view of the display.
	 * @param timeboard,    timeboard to play.
	 * @param piece_list,   list of pieces.
	 * @param special_tile, if the special tile is available.
	 * @param list_menu,    if we are in the list display.
	 * @param width,        width of the screen.
	 * @param height,       height of the screen.
	 * @return int, new state of list menu.
	 */
	private static int action(ApplicationContext context, PatchworkView view, Point2D.Float mouse, Event event, int width,
			int height, int list_menu, User player_turn, User other_player, CircularList piece_list, TimeBoard timeboard) {
		mouse = event.getLocation();
		if (height - height * 0.2 + width * 0.2 + width * 0.25 < mouse.x
				&& mouse.x < height - height * 0.2 + width * 0.2 + width * 0.25 + width * 0.08
				&& height * 0.6 + height * 0.1 < mouse.y && mouse.y < height * 0.1 + height * 0.6 + height * 0.1) {
			list_menu = 1 - list_menu;
			context.renderFrame(graphics -> view.drawAllPieces(graphics));
		} else if (width * 0.05 < mouse.x && mouse.x < width * 0.05 + width * 0.1 && height * 0.25 < mouse.y
				&& mouse.y < height * 0.25 + height * 0.1) {
			timeboard.moveToken(player_turn, other_player, null);
		}
		var piece = detect_clic(mouse.x, mouse.y, context, view, player_turn, piece_list, width, height,
				piece_list.token());
		if (piece != null) {
			player_turn.addButtons(-piece.price());
			piece_list.delete(piece);
			placement(context, view, piece, player_turn);
			timeboard.moveToken(player_turn, other_player, piece.time(), null);
			player_turn.board().addButtons(piece.button());
		}
		return list_menu;
	}

	/**
	 * Detect which piece the player clicked on.
	 * 
	 * @param x,          coordinate x of the mouse.
	 * @param y,          coordinate y of the mouse.
	 * @param context,    graphic context.
	 * @param view,       view of the display.
	 * @param User,       current player.
	 * @param piece_list, list of pieces.
	 * @param width,      width of the screen.
	 * @param height,     height of the screen.
	 * @param index,      index of the piece in the list.
	 * @return Piece, the piece who was clicked on.
	 */
	private static Piece detect_clic(float x, float y, ApplicationContext context, PatchworkView view, User User,
			CircularList piece_list, int width, int height, int index) {

		if (height - height * 0.2 + width * 0.2 < x
				&& x < height - height * 0.2 + width * 0.2 + (width - (height - height * 0.2 + width * 0.2)) / 3
				&& height * 0.2 < y && y < height * 0.2 + (height - height * 0.2) / 4
				&& User.buttons() >= piece_list.get(index).price()) {
			return piece_list.get(index);
		} else if (height - height * 0.2 + width * 0.2 + (width - (height - height * 0.2 + width * 0.2)) / 3 < x
				&& x < height - height * 0.2 + width * 0.2 + 2 * (width - (height - height * 0.2 + width * 0.2)) / 3
				&& height * 0.2 < y && y < height * 0.2 + (height - height * 0.2) / 4
				&& User.buttons() >= piece_list.get(index + 1).price()) {
			piece_list.moveToken(1);
			return piece_list.get(index + 1);
		} else if (height - height * 0.2 + width * 0.2 + 2 * (width - (height - height * 0.2 + width * 0.2)) / 3 < x
				&& x < height - height * 0.2 + width * 0.2 + 3 * (width - (height - height * 0.2 + width * 0.2)) / 3
				&& height * 0.2 < y && y < height * 0.2 + (height - height * 0.2) / 4
				&& User.buttons() >= piece_list.get(index + 2).price()) {
			piece_list.moveToken(2);
			return piece_list.get(index + 2);
		}
		return null;
	}

	/**
	 * Function to place a piece on the quiltboard.
	 * 
	 * @param context, graphic context.
	 * @param view,    view of the display.
	 * @param piece,   the piece to place.
	 * @param User,    the current player.
	 */
	static private void placement(ApplicationContext context, PatchworkView view, Piece piece, User User) {
		var placed = false;
		var x = 0;
		var y = 0;
		do {
			Event event = context.pollOrWaitEvent(10);
			if (event == null)
				continue;

			Action action = event.getAction();
			KeyboardKey key = event.getKey();

			if (action == Action.KEY_PRESSED) {
				if (key.toString().equals("Z") && y > 0)
					y--;
				else if (key.toString().equals("D") && x + piece.body().length < 9)
					x++;
				else if (key.toString().equals("Q") && x > 0)
					x--;
				else if (key.toString().equals("S") && y + piece.body()[0].length < 9)
					y++;
				else if (key.toString().equals("R"))
					piece = piece.rotate();
				else if (key.toString().equals("T"))
					piece = piece.mirror();
				else if (key.toString().equals("SPACE")) {
					if (User.board().place(piece, y, x))
						placed = true;
				}
			}
			PatchworkView.drawPlacement(context, view, piece, x, y, User);
		} while (!placed);
	}
}
